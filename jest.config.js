module.exports = {
  displayName: 'Unit tests',
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'topics',
  testRegex: '\\.spec\\.ts$',
  testPathIgnorePatterns: ['/node_modules/', '.+\\.integration\\.spec\\.ts$'],
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
};
