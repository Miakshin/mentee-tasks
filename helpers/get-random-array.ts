import * as faker from 'faker';

export const getRandomArray = (len) => {
    const arr = [];
    for (let i = 0; i < len; i++) {
        arr.push(faker.random.number());
    }
    return arr;
}
