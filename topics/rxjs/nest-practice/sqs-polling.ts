import {CustomTransportStrategy, Server} from "@nestjs/microservices";
import {Logger} from "@nestjs/common";
import {FakeSqs, IReadFromQueueResponse, IReceiveMessageParams} from "./helpers/fake-sqs";

interface IOptions {
    sqs: FakeSqs;
    logger: Logger;
    pollingInterval: number;
}

// Requirements:
// after method listen executed should start reed from queue
// after method close executed should stop handle messages
// should handle messages one by one in case Messages not empty
// after message handled should execute deleteMessage with queueUrl and ReceiptHandle params

export class SqsTransportStrategy extends Server implements CustomTransportStrategy {
    private readonly sqs: FakeSqs;
    private readonly pollingInterval: number;
    protected readonly logger: Logger;

    constructor(options: IOptions) {
        super();
        const { sqs, logger, pollingInterval } = options;

        this.sqs = sqs;
        this.logger = logger;
        this.pollingInterval = pollingInterval;
    }

    public async listen(callback: () => void): Promise<void> {
        const handlers = this.getHandlers();

        for (const [queue, handler] of handlers.entries()) {
            await this.subscribe(queue, handler);
        }
        callback();
    }

    public close(): void {
        // todo add clean subscriptions
    }

    private readFromQueue(queueUrl: string): Promise<IReadFromQueueResponse> {
        const params: IReceiveMessageParams = {
            QueueUrl: queueUrl,
        };
        return this.sqs.receiveMessage(params);
    }

    private async subscribe(queueUrl: string, handler: (msg: any) => void) {
       // todo add subscriptions to queue (polling)
    }
}
