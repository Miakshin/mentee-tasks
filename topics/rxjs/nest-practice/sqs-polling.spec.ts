import {SqsTransportStrategy} from "./sqs-polling";
import {FakeSqs, IReadFromQueueResponse} from "./helpers/fake-sqs";
import {Logger} from "@nestjs/common";
import {timer} from "rxjs";
import nock from "nock";
import {getFakeSqsMessage} from "./helpers/get-fake-sqs-message";

const mockGetResponseOnce = (url: string, response: any) => {
    nock('').get(url).once().reply(200, response)
};

const getReadFromQueueResponse = (body): IReadFromQueueResponse => ({
    Messages: [body],
});

const getReadFromQueueEmptyResponse = (): IReadFromQueueResponse => ({
    Messages: [],
});

describe('SqsTransportStrategy', () => {

    let strategy: SqsTransportStrategy;
    let fakeSqs: FakeSqs

    beforeEach(() => {
        fakeSqs = new FakeSqs()
        strategy = new SqsTransportStrategy({
            sqs: fakeSqs,
            pollingInterval: 1000,
            logger: new Logger('SqsTransportStrategy'),
        });

        nock.restore();
    });

    it('', async () => {
        const firstQueue = 'http://some-queue.com/handler-1';
        const secondQueue = 'http://some-queue.com/handler-2';
        const firstHandler = jest.fn();
        const secondHandler = jest.fn();

        mockGetResponseOnce(firstQueue, getReadFromQueueResponse(getFakeSqsMessage({ foo: 'bar' }, 'ReceiptHandle1')));
        mockGetResponseOnce(firstQueue, getReadFromQueueResponse(getReadFromQueueEmptyResponse()));
        mockGetResponseOnce(firstQueue, getReadFromQueueResponse(getFakeSqsMessage({ foo: 'bar 3' }, 'ReceiptHandle3')));

        mockGetResponseOnce(secondQueue, getReadFromQueueResponse(getFakeSqsMessage({ bar: 'baz' }, 'ReceiptHandle1')));
        mockGetResponseOnce(secondQueue, getReadFromQueueResponse(getFakeSqsMessage({ bar: 'baz 2' }, 'ReceiptHandle1')));
        mockGetResponseOnce(secondQueue, getReadFromQueueResponse(getFakeSqsMessage({ bar: 'baz 3' }, 'ReceiptHandle3')));

        const handlersMap = new Map<string, (data) => any>();
        handlersMap.set(firstQueue, firstHandler);
        handlersMap.set(secondQueue, secondHandler);

        const deleteMessageSpy = jest.spyOn(fakeSqs, 'deleteMessage');
        jest.spyOn(strategy, 'getHandlers').mockImplementation(() => handlersMap);

        await strategy.listen(console.log);

        await timer(3500).toPromise();
        await strategy.close();
        expect(firstHandler).toHaveBeenCalledTimes(2);
        expect(secondQueue).toHaveBeenCalledTimes(3);
        expect(deleteMessageSpy).toHaveBeenCalledTimes(5);

        expect(firstHandler.mock.calls[0][0]).toMatchSnapshot({ foo: 'bar' });
        expect(firstHandler.mock.calls[1][0]).toMatchSnapshot({ foo: 'bar 3' });

        expect(secondHandler.mock.calls[0][0]).toMatchSnapshot({ bar: 'baz' });
        expect(secondHandler.mock.calls[1][0]).toMatchSnapshot({ bar: 'baz 2' });
        expect(secondHandler.mock.calls[2][0]).toMatchSnapshot({ bar: 'baz 3' });
    });
});
