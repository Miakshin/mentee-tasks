import axios, {AxiosResponse} from "axios";
import {fromPromise} from "rxjs/internal-compatibility";
import {map} from "rxjs/operators";


export interface IReceiveMessageParams {
    QueueUrl: string;
}

export interface  IDeleteMessageParams {
    QueueUrl: string;
    ReceiptHandle: string;
}
export interface IMessage {
    MessageId: string;
    ReceiptHandle: string;
    MD5OfBody: string;
    Body: string;
    Attributes: Record<string, string>
}

export interface IReadFromQueueResponse {
    Messages: IMessage[];
}

export class FakeSqs {
    receiveMessage(params: IReceiveMessageParams): Promise<IReadFromQueueResponse> {
        return fromPromise<AxiosResponse<IReadFromQueueResponse>>(axios.get(params.QueueUrl))
            .pipe(
                map(({data}) => data)
            ).toPromise();
    }

    deleteMessage(params: IDeleteMessageParams) {
        return Promise.resolve(params);
    }
}
