export const getFakeSqsMessage = (body: object, ReceiptHandle = 'gynuykgpmxtofxofhxuoccdfqleamipmbmqndxmvwlmoolewjjencrpwmveksqsqfedteqlxbtlsufrfrbzprqdcilxddlbbebwmeqaihbivjivhvwvbfgbbhtuzkoakimztlvueulzctowtrzsxvluvnjazistyxiyenifggchjemfugzckuyzph') => ({
    MessageId: '0c9c207d-f354-8137-f263-dd8ffa91036c',
    ReceiptHandle,
    MD5OfBody: 'f71597cdb0deaed5cd1f7c5f35deb0a5',
    Body: JSON.stringify({
        Type: 'Notification',
        MessageId: '1ce8e60e-1fb6-47c0-9a27-5cbdf2e6124d',
        Token: null,
        TopicArn: 'arn:aws:sns:us-east-1:000000000000:Some-Topic',
        Message: JSON.stringify(body),
        SubscribeURL: null,
        Timestamp: '2020-09-15T14:46:07.068Z',
        SignatureVersion: 1,
        Signature: 'EXAMPLEpH+..',
        SigningCertURL: 'https://sns.us-east-1.amazonaws.com/Service-Name-0000000000000000000000.pem',
    }),
    Attributes: {
        SenderId: 'AIDAIT2UOQQY3AUEKVGXU',
        SentTimestamp: '1600181167087',
        ApproximateReceiveCount: '1',
        ApproximateFirstReceiveTimestamp: '1600181173835',
    },
});
