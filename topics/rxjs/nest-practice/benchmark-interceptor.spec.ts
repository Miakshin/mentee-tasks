import {LoggingInterceptor} from "./benchmark-interceptor";
import {ExecutionContext, Logger} from "@nestjs/common";
import {of, timer} from "rxjs";

const executionContext = {
    switchToHttp: jest.fn().mockReturnThis(),
    getRequest: jest.fn().mockReturnThis()
} as any as ExecutionContext;

const callHandler = {
    handle: jest.fn()
};

const logger = new Logger('LoggingInterceptor');

describe('LoggingInterceptor', () => {

    const interceptor = new LoggingInterceptor(logger);
    it('should call logger log with correct benchmark message', async () => {
        const timeout = 3000;
        const logSpy = jest.spyOn(logger, 'log');
        jest.spyOn(callHandler, 'handle').mockImplementation(() => timer(timeout));

        await interceptor.intercept(executionContext, callHandler).toPromise();
        expect(logSpy).toHaveBeenCalled();

        const logMessage = logSpy.mock.calls[0][0];
        const [, , time] = logMessage.split(' ');
        expect(time > timeout).toBeTruthy();
    });

    it('should call logger log with correct benchmark message', async () => {
        const logSpy = jest.spyOn(logger, 'log');
        jest.spyOn(callHandler, 'handle').mockImplementation(() => of('execution finished'));

        await interceptor.intercept(executionContext, callHandler).toPromise();

        expect(logSpy).toHaveBeenCalled();
        const logMessage = logSpy.mock.calls[0][0];
        const [, , time] = logMessage.split(' ');
        expect(time > 0).toBeTruthy();
    });
});
