import {Observable} from "rxjs";
import {CallHandler, ExecutionContext, Logger, NestInterceptor} from "@nestjs/common";

export class LoggingInterceptor implements NestInterceptor {

    constructor(private readonly logger: Logger) {
    }

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        // todo implement logging for execution duration in format `request took {time} ms`
    }
}
