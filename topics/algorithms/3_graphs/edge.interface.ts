export interface IEdge {
    from: number;
    to: number;
    weight: number;
}
