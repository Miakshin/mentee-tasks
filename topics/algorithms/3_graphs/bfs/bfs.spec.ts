import {IEdge} from "../edge.interface";
import {getShortestPath, getShortestWay, isPossibleToGet} from "./bfs";

const graph: IEdge[] = [
    { from: 0, to: 1,  weight: 2 },
    { from: 0, to: 2,  weight: 3 },
    { from: 1, to: 4,  weight: 10 },
    { from: 1, to: 3,  weight: 7 },
    { from: 2, to: 1,  weight: 1 },
    { from: 2, to: 3,  weight: 5 },
    { from: 2, to: 5,  weight: 3 },
    { from: 3, to: 4,  weight: 2 },
    { from: 3, to: 6,  weight: 1 },
    { from: 4, to: 6,  weight: 5 },
    { from: 4, to: 7,  weight: 8 },
    { from: 5, to: 3,  weight: 1 },
    { from: 5, to: 6,  weight: 4 },
    { from: 6, to: 5,  weight: 4 },
    { from: 6, to: 7,  weight: 7 },
    { from: 8, to: 9,  weight: 3 },
    { from: 8, to: 10,  weight: 1 },
    { from: 8, to: 11,  weight: 5 },
    { from: 9, to: 10,  weight: 4 },
    { from: 10, to: 12,  weight: 3 },
    { from: 11, to: 12,  weight: 6 },
    ]

describe('isPossibleToGet', () => {
    it('should return true if path exist', async () => {
        expect(isPossibleToGet(graph, 0, 7)).toBe(true);
        expect(isPossibleToGet(graph, 1, 6)).toBe(true);
        expect(isPossibleToGet(graph, 5, 4)).toBe(true);
        expect(isPossibleToGet(graph, 8, 12)).toBe(true);
    });

    it('should return false if path not exist', async () => {
        expect(isPossibleToGet(graph, 0, 10)).toBe(false);
        expect(isPossibleToGet(graph, 12, 0)).toBe(false);
        expect(isPossibleToGet(graph, 7, 6)).toBe(false);
        expect(isPossibleToGet(graph, 3, 8)).toBe(false);
    });
});

describe('getShortestWay', () => {
    it('should return lowest value', async () => {
        expect(getShortestWay(graph, 0, 3)).toBe(7);
        expect(getShortestWay(graph, 8, 12)).toBe(4);
        expect(getShortestWay(graph, 0, 7)).toBe(16);
        expect(getShortestWay(graph, 0, 6)).toBe(9);
    });

    it('should return null if path not exist', async () => {
        expect(getShortestWay(graph, 0, 10)).toBe(null);
        expect(getShortestWay(graph, 12, 0)).toBe(null);
        expect(getShortestWay(graph, 7, 6)).toBe(null);
        expect(getShortestWay(graph, 3, 8)).toBe(null);
    });
});

describe('getShortestPath', () => {
    it('should return shortest way', async () => {
        expect(getShortestPath(graph, 0, 3)).toStrictEqual([0, 2 , 5 ,3]);
        expect(getShortestPath(graph, 8, 12)).toStrictEqual([8, 10 ,12]);
        expect(getShortestPath(graph, 0, 7)).toStrictEqual([0, 2, 5, 3, 6, 7]);
        expect(getShortestPath(graph, 0, 6)).toStrictEqual([0, 2, 5, 3, 6]);
    });

    it('should return null if path not exist', async () => {
        expect(getShortestPath(graph, 0, 10)).toBe(null);
        expect(getShortestPath(graph, 12, 0)).toBe(null);
        expect(getShortestPath(graph, 7, 6)).toBe(null);
        expect(getShortestPath(graph, 3, 8)).toBe(null);
    });
});
