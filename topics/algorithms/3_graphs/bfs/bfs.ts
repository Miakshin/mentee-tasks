import {IEdge} from "../edge.interface";

export const isPossibleToGet = (graph: IEdge[], start: number , end: number) => {
    // todo implement function that checks if the path from start to end position exists in the graph
}

export const getShortestWay = (graph: IEdge[], start: number , end: number) => {
    // todo implement function that returns minimal sum of weight for graph
}

export const getShortestPath = (graph: IEdge[], start: number , end: number) => {
    // todo implement function that returns minimal shortest way from start to end vertexes
}
