# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `yarn` command
2. Run `docker-compose up` command

## Before run project:
* Fill entities
* Generate migration (`yarn typeorm:cli migration:generate -n MigrationName`)
* Run migration (`yarn typeorm:cli migration:run`)

PS. Main purpose of this task - implement structure for storing companies with its structure (departments);
Requirements:
- Company can have departments
- Department can have sub-departments
- It's possible to build tree structure
- Max depth of the tree - 4
