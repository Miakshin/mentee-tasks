export const quickSort = (arr: number[]) => {
  if (arr.length < 2) return arr;

  const pivot = Math.floor(arr.length / 2);
  const left = [];
  const right = [];

  arr.forEach((el, index) => {
    if (index === pivot) return;
    el > arr[pivot] ? right.push(el): left.push(el)
  })

  return [...quickSort(left), arr[pivot], ...quickSort(right)];
};
