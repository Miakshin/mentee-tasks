const merge = (leftArray: number[], rightArray: number[]) => {
    const array = [];
    let leftIndex = 0
    let rightIndex = 0

    while(leftIndex < leftArray.length && rightIndex < rightArray.length) {
        if(leftArray[leftIndex] > rightArray[rightIndex]) {
            array.push(rightArray[rightIndex]);
            rightIndex++;
        } else {
            array.push(leftArray[leftIndex])
            leftIndex++;
        }
    }

    return [...array, ...leftArray.slice(leftIndex), ...rightArray.slice(rightIndex)];
}

export const mergeSort = (array: number[]) => {
    if (array.length < 2) {
        return array;
    }

    const midst = Math.floor(array.length / 2);

    return merge(mergeSort(array.slice(0, midst)), mergeSort(array.slice(midst)));
};
