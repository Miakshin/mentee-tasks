import {mergeSort} from "./merge-sort";
import {sortBy} from 'lodash';
import {getRandomArray} from "../../../helpers/get-random-array";

describe('mergeSort', () => {
    it('should sort array with numbers', async () => {
        const dataset1 = getRandomArray(10000);
        const dataset2 = getRandomArray(100000);
        const dataset3 = getRandomArray(1000000);
        expect(mergeSort(dataset1)).toStrictEqual(sortBy(dataset1));
        expect(mergeSort(dataset2)).toStrictEqual(sortBy(dataset2));
        expect(mergeSort(dataset3)).toStrictEqual(sortBy(dataset3));
    });

    it('should create new array and not modify existed', async () => {
        const arr = [1, 3, 2, 5, 12, 10, 22, 1, 33, 13];
        expect(mergeSort(arr)).toStrictEqual([1, 1, 2, 3, 5, 10, 12, 13, 22, 33]);
        expect(arr).toStrictEqual([1, 3, 2, 5, 12, 10, 22, 1, 33, 13])
    });
});
