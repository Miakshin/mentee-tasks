export const selectionSort = (arr: number[]) => {
    const result = [arr[0]];

    for (let i = 1; i < arr.length; i++) {
        let j = result.length - 1;

        if (j === 0) {
            arr[i] < result[j] ? result.splice(0, 0, arr[i]) : result.push(arr[i])
        } else {
            while (arr[i] < result[j] && j > 0) {
                j--;
            }

            result.splice((arr[i] < result[j] && j === 0 ? j : j + 1), 0, arr[i]);
        }
    }

    return result;
};
