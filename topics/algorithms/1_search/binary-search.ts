export const binarySearch = (array: number[], value) => {
    const midst = Math.floor(array.length / 2);

    if (array[midst] === value) {
        return array[midst]
    }

    if (!array.length) {
        return;
    }

    return array[midst] < value ? binarySearch(array.slice(midst + 1), value) : binarySearch(array.slice(0, midst), value)
}
