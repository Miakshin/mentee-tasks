import { binarySearch } from "./binary-search";

describe('binarySearch', () => {
    const cases = [
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110],
        [10, 25, 30, 44, 50, 66, 70, 88, 99, 119, 121],
        [1, 22, 23, 44, 45, 46, 57, 57, 59, 100, 110],
        [-100, -21, -2, 21, 3, 40, 40, 40, 57, 68, 69, 100],
        [1, 2, 3, 402, 406, 412, 412, 468, 569, 600, 601],
    ]


    it('should return value if it presented in array', async () => {
        expect(binarySearch(cases[0], 3)).toBe(3);
        expect(binarySearch(cases[1], 50)).toBe(50);
        expect(binarySearch(cases[2], 25)).toBe(25);
        expect(binarySearch(cases[3], 22)).toBe(22);
        expect(binarySearch(cases[4], -21)).toBe(-21);
        expect(binarySearch(cases[5], 412)).toBe(412);
    });

    it('should return null if value not presented in array', async () => {
        expect(binarySearch(cases[0], 12)).toBe(undefined);
        expect(binarySearch(cases[1], 55)).toBe(undefined);
        expect(binarySearch(cases[2], 300)).toBe(undefined);
        expect(binarySearch(cases[3], -1)).toBe(undefined);
        expect(binarySearch(cases[4], -150)).toBe(undefined);
        expect(binarySearch(cases[5], 403)).toBe(undefined);
    });
});
