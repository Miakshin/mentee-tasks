# Tasks repository

This repository contains tasks for particular topics. Main purpose of this repo to exercises with mentee.

## List of covered topics:
* [Algorithms](/topics/algorithms)
* [RxJs (TBD)](/topics/rxjs)
* [Node.JS streams (TBD)](/topics/streems)

## Tests

To run tests execute
```shell
yarn test 
```
or 
```shell
yarn test:watch
```
to work in watch mode
